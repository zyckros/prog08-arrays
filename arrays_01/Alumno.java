import java.util.*;

/**
 * Clase Alumno
 */
public class Alumno {

    private String phone = "";
    private String nombre = "";

    public Alumno(String name) {
    }

    public Alumno(String nombre, String phone) {

        this.phone = phone;
        this.nombre = nombre;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
