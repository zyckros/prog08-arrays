import java.util.Scanner;

/**
 * Clase menu donde estan los menu para interactuar con el usuario
 */

public final class Menu {


    public static Agenda agenda = new Agenda();
    public static Scanner scan = new Scanner(System.in);
    static int count = 0;

    /**
     * Esta funcion muestra el menu princpial al usuario
     */
    public static void menuprincipal() {

        int option = 0;

        do {
            try {
                System.out.println("Bienvenido al menu");
                System.out.println("1 - Añadir alumno\n" + "2 - Consultar telefono alumno\n" + "3 - salir");
                option = scan.nextInt();
            } catch (Exception e) {
                scan.nextLine();
                option = 0;
            }

            menuOpcion(option);

        } while (option != 3);

    }

    /**
     * Esta funcion recibe un entero por que sera la opcion que haya seleccionado el usuario
     *
     * @param option
     */
    public static void menuOpcion(int option) {

        System.out.println(count);
        switch (option) {

            case 1:
                count = menuAñadirAlumno(count);
                break;
            case 2:
                consulaTelefono();
                break;
            case 3:
                System.out.println("Hasta pronto!");
                break;
            default:
                System.out.println("El valor introducido es erroneo");
        }


    }

    public static int menuAñadirAlumno(int count) {

        String name = "";
        String phone = "";

        if (count < 2) {

            System.out.println("Nombre del alumno:");
            name = scan.next();
            System.out.println("Telefono del alumno:");
            phone = scan.next();

            agenda.añadirAlumno(name, phone);

            count++;

        } else {
            System.out.println("No hay mas espacio para añadir alumnos");
        }

        return count;
    }

    public static void consulaTelefono() {

        String nombre = "";

        System.out.println("Nombre del alumno: ");
        nombre = scan.next();

        agenda.buscarTelefono(nombre);

    }


}
