import java.util.ArrayList;

public class Agenda {

    private ArrayList<Alumno> agenda = new ArrayList<Alumno>();


    /**
     * Esta funcion recibe por parametro dos String "name" y "phone" para crear y añadir objetos de Alumno al ArrayList de Alumno.
     *
     * @param name
     * @param phone
     */
    public void añadirAlumno(String name, String phone) {
        agenda.add(new Alumno(name, phone));

        for (Alumno e : agenda) {
            System.out.println("Nombre: " + e.getNombre() + "Telefono: " + e.getPhone());
        }
    }


    /**
     * Esta funcion recibe una String por parametro "nombre" y compara con cada objeto en el ArrayList de alumno mostrar el telefono.
     *
     * @param nombre
     */

    public void buscarTelefono(String nombre) {

        String telefono = "";

        for (int i = 0; i < agenda.size(); i++) {

            String comparar = agenda.get(i).getNombre();

            if (nombre.equals(comparar)) {
                System.out.println("El telefono es: " + agenda.get(i).getPhone());
                telefono = agenda.get(i).getPhone();
            }
        }
        if (telefono == "") {
            System.out.println("no se ha encontrado el alumno");
        }
    }

}
