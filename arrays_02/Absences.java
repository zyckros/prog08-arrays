import java.util.Arrays;
import java.util.Scanner;

public final class Absences {

    private static String[] names = new String[10];
    private static String[] namesCourse = new String[3];
    private static int[][] absences = new int[10][3];
    static Scanner scan = new Scanner(System.in);


    /**
     * This function fill all data
     */
    public static void loadTable() {

        for (int i = 1; i <= namesCourse.length; i++) {
            System.out.println("Introduce la asignatura " + i);
            String asignature = scan.nextLine();
            namesCourse[i - 1] = asignature;
        }

        for (int i = 0; i < names.length; i++) {

            System.out.println("Introduce el alumno " + (i + 1));
            String name = scan.nextLine();
            names[i] = name;

            for (int j = 0; j < absences[i].length; j++) {

                boolean confirmAbsenceIsNumber;

                do {
                    System.out.println("introduce el numero de faltas para " + names[i] + " en " + namesCourse[j]);
                    try {
                        confirmAbsenceIsNumber = true;
                        int absence = scan.nextInt();
                        absences[i][j] = absence;
                    } catch (Exception e) {
                        scan.nextLine();
                        confirmAbsenceIsNumber = false;
                    }

                } while (!confirmAbsenceIsNumber);
            }
            scan.nextLine();
        }

    }


    /**
     * This function show the faults of a studens in a course
     */
    public static void consultFaultsForNameAndCourse() {

        int indexName = 0;
        int indexCourse = 0;
        boolean statusName = false;
        boolean statusCourse = false;

        System.out.println("Introduce el nombre del alumno: ");
        String name = scan.nextLine();
        System.out.println("Introduce el nombre del curso: ");
        String course = scan.nextLine();

        for (int i = 0; i < names.length; i++) {

            if (name.equalsIgnoreCase(names[i])) {
                indexName = i;
                statusName = true;
                break;
            }

        }
        for (int i = 0; i < namesCourse.length; i++) {

            if (course.equalsIgnoreCase(namesCourse[i])) {
                indexCourse = i;
                statusCourse = true;
                break;
            }
        }

        if (statusCourse && statusName) {
            System.out.println("Las faltas para " + names[indexName] + " en el curso " + namesCourse[indexCourse] + " son " + absences[indexName][indexCourse]);
        } else {
            System.out.println("No se ha encontrado al alumno o la asignatura, vuelva a intentarlo");
        }


    }


    /**
     * This function show student with more faults
     */
    public static void consultStudentForMoreFaults() {

        int finalResult = 0;
        int compare = 0;
        int index = 0;

        for (int i = 0; i < absences.length; i++) {

            for (int j = 0; j < absences[i].length; j++) {

                compare += absences[i][j];
            }

            if (compare > finalResult) {
                finalResult = compare;
                index = i;
            }

            compare = 0;
        }

        System.out.println(names[index] + " es el alumno con mayor numero de faltas, la cantidad de faltas son " + finalResult);


    }

    /**
     * This function clone names array and show
     */
    public static void orderForNames() {

        try {
            String[] orderNames = new String[10];
            orderNames = names.clone();
            Arrays.sort(orderNames);
            for (int i = 0; i < names.length; i++) {
                System.out.println(orderNames[i]);
            }
        } catch (Exception e) {
            System.out.println("Se ha producido esta excepcion ---> " + e);

        }

    }


    /**
     * This function show all faults of a certain student
     */
    public static void consultAllFaults() {

        System.out.println("Introduce el alumno: ");
        String name = scan.nextLine();
        int index = 0;
        int allabcenses = 0;
        boolean status = false;

        for (int i = 0; i < names.length; i++) {

            if (name.equalsIgnoreCase(names[i])) {
                index = i;
                allabcenses = absences[i][0] + absences[i][1] + absences[i][2];
                status = true;
                break;
            }

        }

        if (status) {
            System.out.println("el total de faltas para " + names[index] + " en las tres asignaturas son " + allabcenses);
        } else {
            System.out.println("No se ha encontrado al alumno");
        }
    }

}
