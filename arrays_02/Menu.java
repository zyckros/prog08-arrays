import java.util.Scanner;

public final class Menu {

    static Scanner scan = new Scanner(System.in);

    /**
     * This function show option panel
     */
    public static void mainMenu() {

        int option = 0;

        do {

            System.out.println("\n1 - cargar tabla");
            System.out.println("2 - Consulta del numero de faltas para un alumno y una asignatura determinada");
            System.out.println("3 - Consulta del alumno con mayor numero de faltas");
            System.out.println("4 - Ordenar la tabla por nombre");
            System.out.println("5 - Consulta del total de faltas para un alumno dado en las tres asgnaturas");
            System.out.println("6 - salir");

            try {
                option = scan.nextInt();
            } catch (Exception e) {
                System.out.println(e);
                scan.nextLine();
                option = 0;
            }

            selectedOption(option);

        } while (option != 6);
    }


    /**
     * This function takes as parameter the option selected by the user and call functions in Absences classa
     *
     * @param option - int
     */
    public static void selectedOption(int option) {

        switch (option) {

            case 1:
                Absences.loadTable();
                break;
            case 2:
                Absences.consultFaultsForNameAndCourse();
                break;
            case 3:
                Absences.consultStudentForMoreFaults();
                break;
            case 4:
                Absences.orderForNames();
                break;
            case 5:
                Absences.consultAllFaults();
                break;
            case 6:
                System.exit(0);
                break;
        }
    }
}