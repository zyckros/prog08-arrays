public class Student {

    String name = "";
    String address = "";
    String dni = "";
    String phone = "";


    public Student() {

    }

    public Student(String name, String address, String dni, String phone) {
        this.name = name;
        this.address = address;
        this.dni = dni;
        this.phone = phone;
    }


    public void StringconsultDataStudent() {

        System.out.println(" Name: " +  this.name + "  Address: " + this.address + "  DNI: " + this.dni + "  Phone: " + this.phone);

    }

}
